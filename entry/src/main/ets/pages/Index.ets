/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CardManager } from '../manager/CardManager';
import promptAction from '@ohos.promptAction';
import router from '@ohos.router';

@Extend(Button)
function btnStyle() {
  .width(300)
  .height(80)
  .margin(10)
}

export const CARD_FILE_PATH = 'rawfile/card_demo.hap';

export const CARD_FILE_NAME = 'card_demo';

export const CARD_ENTRY = 'ets/pages/Index';

@Entry
@Component
struct Index {
  aboutToAppear(): void {
    CardManager.getInstance().init(getContext(this));
  }

  build() {
    Column() {
      Button('Register Card Package').btnStyle().onClick(() => this.registerCards(getContext(this)))
      Button('IsolatedComponent Page').btnStyle().onClick(() => router.pushUrl({ url: 'pages/IsolatedComponentPage' }))
    }
    .width('100%')
    .height('100%')
  }

  async registerCards(context: Context) {
    context.resourceManager.getRawFileContent(CARD_FILE_PATH)
      .then(binFile => CardManager.getInstance().registerCardStream(CARD_FILE_NAME, binFile))
      .then(() => promptAction.showToast({ message: 'register card package success' }))
      .catch((e: Error) => promptAction.showToast({ message: e.message }));
  }
}