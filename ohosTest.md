|测试功能|预置条件|输入|预期输出|是否自动|测试结果|
|-------|-------|----|-------|--------|-------|
|加载和注册云卡|成功安装hap|点击Register Card Package按钮|taost提示："register card package success"|否|pass|
|拉起云卡|成功加载和注册云卡|点击IsolatedComponent Page按钮|成功打开新页面，并显示一个文本和两个按钮|否|pass|
|点击事件测试|成功拉起云卡|点击页面中Button1按钮|文本框显示Button1: onClick|否|pass|
|键盘事件测试|成功拉起云卡，连接键盘|按TAB键|焦点切换到Buttone1按钮,文本框显示：Button1: onKeyEvent|否|pass|