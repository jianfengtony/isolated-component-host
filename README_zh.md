# IsolatedComponent

### 介绍

本示例主要展示IsolatedComponent的基本能力，通过该组件在受限worker上加载独立发布的卡片hap包，并实现了简单的卡片包管理的功能。
依赖了关键接口:
1. [@ohos.bundle.bundleManager](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis-ability-kit/js-apis-bundleManager.md#bundlemanagerverifyabc11)

### 效果预览

|   Index Page                          | IsolatedComponent Page                             |
|---------------------------------------|----------------------------------------------------|
| ![image](screenshots/index_page.jpeg) | ![image](screenshots/isolated_component_page.jpeg) |

使用说明

1. IsolatedComponentHost为需要安装在手机上的应用（下文称为Host应用），提供了动态加载卡片hap包的能力；
2. IsolatedCard为被Host应用加载的卡片包工程（下文称为卡片包）。编译Host应用前，需要先编译卡片包，然后将编译后生成的卡片hap文件（一般情况下应用编译后会生成签名和非签名两个hap，这里不区分，任意hap均可运行），拷贝到Host应用的entry模块的src/main/resources/rawfile下面（如果rawfile目录不存在可自行创建一个），并将文件命名为 card_demo.hap;
3. 编译Host工程，并将生成的hap包安装到设备中；
4. 打开Host应用，在Index Page页，可以点击Register Card Package按钮，应用会将资源文件中预置的hap包拷贝到应用的沙箱路径下，并将hap文件解压后将ABC文件校验后保存在系统安全路径下；
5. 点击IsolatedComponent Page按钮，应用会打开一个新的页面，创建一个受限worker，并调用IsolatedComponent的接口，在新的页面中渲染已经注册的hap包。

### 工程目录

```
entry/src/main/ets/
|---manager
|   |---CardManager.ets                    // 卡片包管理模块
|   |---CardMeta.ets                       // 卡片包元数据
|---pages
|   |---Index.ets                          // 首页
|   |---IsolatedComponentPage.ets          // 卡片页面
|---workers
|   |---Worker.ets                         // worker运行的入口文件
```

### 具体实现

* Index页面提供了注册卡片和拉起IsolatedComponent页面的入口，源码参考：[Index.ts](entry/src/main/ets/pages/Index.ets)

* IsolatedComponentPage页面提供了worker和IsolatedComponent创建及拉起的功能，源码参考：[IsolatadComponentPage](entry/src/main/ets/pages/IsolatedComponentPage.ets)
    * 使用了worker.RestrictedWorker来创建运行卡片ABC代码的运行时环境；
    * 使用了IsolatedComponent来加载ABC和渲染UI；

* 对hap文件的管理功能封装在了CardManager模块中，提供了卡片hap包解压缩、校验ABC可执行文件、注册和管理元数据的功能，源码参考：[CardManager.ets](entry/src/main/ets/manager/CardManager.ets)
    * 使用了系统的zlib库对hap压缩包进行解压；
    * 使用了系统的bundleManager接口对解压出来的ABC文件进行校验；

### 相关权限

[ohos.permission.RUN_DYN_CODE](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/security/AccessToken/permissions-for-all.md#ohospermissionrun_dyn_code)

### 依赖

Host应用依赖与本工程同级目录下的IsolatedCard工程作为动态加载的卡片工程来运行。

### 约束与限制

1. 本示例仅支持标准系统上运行，支持设备：RK3568;
2. 本示例仅支持API12版本SDK，版本号：5.0.0.26，镜像版本号：OpenHarmony 5.0.0.26。
3. 本示例需要使用DevEco Studio 4.1 Release (Build Version: 4.1.0.400)才可编译运行；

### 下载

如需单独下载本工程，执行如下命令：

```sh
git init
git config core.sparsecheckout true
echo code/SuperFeature/IsolatedComponent/ > .git/info/sparse-checkout
git remote add origin https://gitee.com/openharmony/applications_app_samples.git
git pull origin master
```